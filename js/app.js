var app = angular.module('myApp', []);


var phoneNumberCollection = {
    areaCodes: [],
    middleThree: [],
    lastFour: []
}

function pad(num, size) {
    var s = num+"";
    while (s.length < size) s = 0 + s;
    return s;
}

for (var i = 0; i < 999; i++) {
    var numString = pad(i, 3);
    phoneNumberCollection.areaCodes.push(numString);
    phoneNumberCollection.middleThree.push(numString);
}

for (var i = 0; i < 9999; i++) {
    var numString = pad(i, 4);
    phoneNumberCollection.lastFour.push(numString);
}

//console.log(phoneNumberCollection);

app.controller('PageController', function() {
    //
    // Initials Entry
    //
    this.date = Date.now();
    this.number = 0;
    this.incrementNumber = function(num) { this.number += num };

    this.initialsText = {
        0: '-',
        1: '-',
        2: '-'
    };
    this.currentInitial = 0;
    this.currentCharacter = 0;
    this.characters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 
    'J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','!',' '];

    this.setCharacters = function() {
        this.initialsText[this.currentInitial] = this.characters[this.currentCharacter];
    };

    this.incrementCharacter = function() { 
        this.currentCharacter++;
        if (this.currentCharacter > this.characters.length - 1) {
            this.currentCharacter = 0;
        }
        this.setCharacters();
    };

    this.decrementCharacter = function() { 
        this.currentCharacter--; 
        if (this.currentCharacter < 0) {
            this.currentCharacter = this.characters.length - 1;
        }
        this.setCharacters();
    };

    this.nextInitial = function() {
        this.currentInitial++;
        this.currentCharacter = 0;
        this.setCharacters();
    };

    //
    // Phone number entry
    //
    this.phoneNumbers = phoneNumberCollection;

});